package me.zyrkran.planets.navigator;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.zyrkran.planets.Main;
import me.zyrkran.planets.config.MainConfig;

public class Navigator {
	
	private static MainConfig config;
	private static Navigator navigator;
	private static Inventory inventory;

	static List<NavigatorButton> buttons;
	
	private Navigator() {
		config = Main.getMainConfig();
		inventory = Bukkit.createInventory(null, 36, "SpaceAge Planets!");
		buttons = new ArrayList<>();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				inventory.clear();
				updateNavigator(); // update navigator buttons every 5 seconds
			}
			
		}, 0, 100);
	}
	
	private void updateNavigator() {
		config.reloadConfig();
		for (String string : Main.getInstance().getConfig().getConfigurationSection("navigator").getKeys(false)) {			
			
			String server 		= config.getString("navigator." + string + ".server");
			String displayname 	= config.getString("navigator." + string + ".inventory-settings.displayname");
			String material 	= config.getString("navigator." + string + ".inventory-settings.material");
			String slot			= config.getString("navigator." + string + ".inventory-settings.slot");
			List<String> lore	= config.getStringList("navigator." + string + ".inventory-settings.description");

			ItemStack item = new ItemStack(Material.valueOf(material));
			
			NavigatorButton button = new NavigatorButton(item, Integer.valueOf(slot));
			button.setName(string);
			button.setDisplayname(displayname);
			button.setLore(lore);
			button.setDestination(server);
			
			inventory.setItem(button.getSlot(), button.getItemStack());
			buttons.add(button);
		}
	}
	
	public void open(Player player) {
		player.openInventory(inventory);
	}
	
	public Inventory getInventory() {
		return inventory;
	}
	
	public List<NavigatorButton> getButtons(){
		return buttons;
	}
	
	public static Navigator getNavigator() {
		if (navigator == null) {
			navigator =  new Navigator();
		}
		
		return navigator;
	}
}
