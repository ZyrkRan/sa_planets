package me.zyrkran.planets.navigator;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.zyrkran.planets.SocketManager;
import me.zyrkran.planets.listeners.ServerDataListener;
import net.md_5.bungee.api.ChatColor;

public class NavigatorButton{
	
	private String name, displayname, server;
	private List<String> lore;
	
	private int slot;
	private ItemStack item;
	
	public NavigatorButton(ItemStack item, int slot) {
		this.item = item;
		this.slot = slot;
	}
	
	public void setDestination(String server) {
		this.server = server;
	}
	
	public String getDestination() {
		return server;
	}
	
	public void setDisplayname(String displayname) {
		this.displayname = color(displayname);
	}
	
	public String getDisplayname() {
		return displayname;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setLore(List<String> lore) {
		this.lore = lore;
	}
	
	public List<String> getLore(){
		return lore;
	}
	
	public ItemStack getItemStack() {
		ItemMeta meta = item.getItemMeta();
		
		List<String> coloredLore = new ArrayList<>();
		for (String s : lore) {
			/* Parse placeholders */
			s = s.replace("{planet_state}", ServerDataListener.getPlanetState(name) + "");
			s = s.replace("{online_players}", ServerDataListener.getPlayerCount(name) + "");
			s = s.replace("{online_status}",  ServerDataListener.getPlanetStatus(name) ? "online" : "offline");
			s = s.replace("{last_restore}",   ServerDataListener.getLastRestore(name) + "");
			
			coloredLore.add(color(s));
		}
		
		meta.setDisplayName(color(name));
		meta.setLore(coloredLore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		item.setItemMeta(meta);
		return item;
	}
	
	public int getSlot() {
		return slot;
	}
	
	private String color(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}
	
	private String decolor(String string) {
		return ChatColor.stripColor(string);
	}

	public static NavigatorButton fromSlot(int slot) {
		for (NavigatorButton b : Navigator.getNavigator().getButtons()) {
			if (b.getSlot() == slot) {
				return b;
			}
		} return null;
	}
	
	public void onClick(Player player) {
		final boolean online = ServerDataListener.getPlanetStatus(name);
        if (!online) {
            player.sendMessage(ChatColor.RED + "Can't join server right now because it appears offline!");
            return;
        }
        
        SocketManager.sendData("SA_Planets", "CONNECT;" + player.getUniqueId() + ";" + decolor(name));
	}
}
