package me.zyrkran.planets;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.rhaz.socket4mc.Bukkit;
import fr.rhaz.socketapi.SocketAPI.Client.SocketClient;

public class SocketManager implements Listener {

	private static SocketClient client;
    
    public static void sendData(final String channel, final String data) {
    	if (client == null) {
    		return;
    	}
        getSocketClient().writeJSON(channel, data);
    }
    
    public static void connectPlayer(Player player, String server) {
    	sendData("SA_Planets", "CONNECT;" + player.getUniqueId() + ";" + server);
    }
    
    public static SocketClient getClient() {
    	return client;
    }
    
    private static SocketClient getSocketClient() {
        return client;
    }
    
    @EventHandler
    private void onHandShake(Bukkit.BukkitSocketHandshakeEvent event) {
        client = event.getClient();
    }
}
