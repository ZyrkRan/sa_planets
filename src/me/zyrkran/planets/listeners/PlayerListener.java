package me.zyrkran.planets.listeners;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;

import me.zyrkran.planets.navigator.Navigator;
import me.zyrkran.planets.navigator.NavigatorButton;

public class PlayerListener implements Listener	{
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		
		if (event.getClick().equals(ClickType.LEFT)) {
			if (event.getInventory().equals(Navigator.getNavigator().getInventory())) {
				event.setCancelled(true);
				
				NavigatorButton button = NavigatorButton.fromSlot(event.getSlot());
	            if (button == null) {
	                return;
	            }
	            
	            // let the button decide
	            button.onClick(player);

				// finally, play sound
				player.playSound(player.getLocation(), Sound.BLOCK_NOTE_PLING, 1F, 1F);
			}
		}
	}
}
