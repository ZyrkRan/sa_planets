package me.zyrkran.planets.listeners;

import java.util.*;
import me.zyrkran.planets.Main;
import me.zyrkran.planets.WorldHandler;
import me.zyrkran.planets.navigator.*;

import org.bukkit.Bukkit;
import org.bukkit.entity.*;
import org.bukkit.event.*;

import fr.rhaz.socket4mc.Bukkit.BukkitSocketJSONEvent;

public class ServerDataListener implements Listener {
	
    private static HashMap<String, Boolean> planet_status = new HashMap<>();
    private static HashMap<String, Integer> planet_players = new HashMap<>();
    private static HashMap<String, String>  planet_state = new HashMap<>();
    private static HashMap<String, String>  planet_last_restore = new HashMap<>();

	public static String getPlanetState(String planetName) {
		if (planet_state.containsKey(planetName)) {
			return planet_state.get(planetName);
		}
		return null;
	}
	
	public static String getLastRestore(String planetName) {
		if (planet_last_restore.containsKey(planetName)) {
			return planet_last_restore.get(planetName);
		}
		return null;
	}
    
    public static boolean getPlanetStatus(String planetName) {
    	if (planet_status.containsKey(planetName)) {
    		return planet_status.get(planetName);
    	}
        return false; 
    }
    
    public static int getPlayerCount(String planetName) {
        if (planet_players.containsKey(planetName)) {
        	return planet_players.get(planetName);
        }
        return 0;
    }
    
    @EventHandler
    public void onSocketJSONMessage(BukkitSocketJSONEvent event) {
        String channel = event.getChannel();
        
        System.out.println(String.valueOf(channel) + " / " + event.getData());
        
        if (channel.equalsIgnoreCase("SA_Planets")) {
            String command = event.getData().split(";")[0];
            String[] data = event.getData().split(";");
            
            // OPEN_NAVIGATOR;PLAYER_UUID
            if (command.equalsIgnoreCase("OPEN_NAVIGATOR")) {
                Player player = Bukkit.getPlayer(UUID.fromString(data[1]));
                Navigator.getNavigator().open(player);
            }
            
            // PLAYER_COUNT;SERVER_NAME;INT_COUNT
            else if (command.equalsIgnoreCase("PLAYER_COUNT")) {
                planet_players.put(data[1], Integer.valueOf(data[2]));
            }
            
            // PLANET_STATUS;SERVER_NAME;BOOL_STATUS
            else if (command.equalsIgnoreCase("PLANET_STATUS")) {
                planet_status.put(data[1], Boolean.valueOf(data[2]));
            }
            
            // PLANET_STATE;SERVER_NAME:STRING_STATE
            else if (command.equalsIgnoreCase("PLANET_STATE")) {
            	planet_state.put(data[1], data[2]);
            }
            
            // RESTORE_PLANET;SERVER_NAME
            else if (command.equalsIgnoreCase("RESTORE_PLANET")) {
            	event.write("RESTORE_PLANET_CONFIRM;" + data[1]);
            	WorldHandler.initRestore(Main.getInstance());      
            }
            
            // RESTORE_LAST;SERVER_NAME;STRING_DATA
            else if (command.equalsIgnoreCase("RESTORE_LAST")) {
            	System.out.println(data[1] + " / " + data[2]);
            	planet_last_restore.put(data[1], data[2]);
            }
        }
    }

}
