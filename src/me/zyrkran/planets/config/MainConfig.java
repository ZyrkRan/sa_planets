package me.zyrkran.planets.config;

import java.util.List;

import org.bukkit.plugin.java.JavaPlugin;

public class MainConfig extends Config{

	public MainConfig(JavaPlugin plugin, String fileName) {
		super(plugin, fileName);
	}
	
	public String getString(String path) {
		return getConfig().getString(path);
	}
	
	public List<String> getStringList(String path){
		return getConfig().getStringList(path);
	}

}
