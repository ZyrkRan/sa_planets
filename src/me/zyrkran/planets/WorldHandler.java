package me.zyrkran.planets;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;

public class WorldHandler {
	
	public static void initRestore(Main plugin) {
		delete();
		
		// start copying after 2 seconds
		Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {
			
			@Override
			public void run() {

				copy();
				
				// shutdown server after 2 seconds
				Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
					
					@Override
					public void run() {

						Bukkit.shutdown(); // restart server

					}
				}, 40);
				
			}
		}, 40);
	}
	
	
	private static void copy() {    
		String rootPath = Bukkit.getWorldContainer().getAbsolutePath();
		
        File srcDir = new File(rootPath + "/world_reset");
        if (!srcDir.exists()) {
            System.out.println("Backup does not exist!");
            return;
        }
        
        File destDir = new File(rootPath + "/world");
        try {
            FileUtils.copyDirectory(srcDir, destDir);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        Bukkit.getServer().createWorld(new WorldCreator("world"));
	}
	
	private static void delete() {
		Bukkit.getServer().unloadWorld("world", false);
        File dir = new File(Bukkit.getServer().getWorld("world").getWorldFolder().getPath());
        try {
            FileUtils.deleteDirectory(dir);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
	}
	
//	private static void deleteWorld(File folder) {
//		for (File file : folder.listFiles()) {
//			if (file.isDirectory()) {
//				File[] children = file.listFiles();
//				for (File child : children) {
//					child.delete();
//				}
//			}
//			file.delete();
//		}folder.delete(); // delete top folder
//	}
//	
//	private static void copy(File source, File dest) {
//		try {
//			FileUtils.copyDirectory(source, dest);
//			
//			// debug
//			System.out.println("Copied \"" + source.getAbsolutePath() + "\" to \"" + dest.getAbsolutePath() + "\"");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
}
