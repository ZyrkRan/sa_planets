package me.zyrkran.planets;

import org.bukkit.plugin.java.JavaPlugin;

import me.zyrkran.planets.config.MainConfig;
import me.zyrkran.planets.listeners.PlayerListener;
import me.zyrkran.planets.listeners.ServerDataListener;

public class Main extends JavaPlugin {
	
	private static Main instance;
	private static MainConfig config;
	
	public void onEnable() {
		instance = this;
		
		config = new MainConfig(this, "config.yml");
		config.saveDefaultConfig();
		
		getServer().getPluginManager().registerEvents(new SocketManager(), this);
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		getServer().getPluginManager().registerEvents(new ServerDataListener(), this);	
	}
	
	public void onDisable() {
		instance = null;
	}
	
	public static Main getInstance() {
		return instance;
	}
	
	public static MainConfig getMainConfig() {
		return config;
	}
	
	public static void saveMainConfig() {
		config.saveConfig();
	}
}
